package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_006_UpdateProfile extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_006_UpdateProfile.class);
	File getEmail, getPhone, getId, getName, getPassword, getStaticEmail, getStaticPhone;
	Scanner emailReader, phoneReader, idReader, nameReader, passwordReader, staticEmailReader, staticPhoneReader;
	String baseUrl = "";
	String endpointTC01 = "/login";
	String endpointUpdate = "/";
	String randomPhoneNumber = getRandomPhone();
	String existingEmail = "";
	String existingPhone = "";
	String existingId = "";
	String existingUserName = "";
	String existingUserPassword = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String staticEmail = "";
	String staticPhone = "";
	
//	String emailAfterUpdate = "";
//	String passwordAfterUpdate = "";
//	String phoneAfterUpdate = "";
//	String nameAfterUpdate = "";
	
	String responseDescriptionInvalidEMail = "invalid email address";
	String responseDescriptionInvalidPassword = "invalid password";
	String responseDescriptionEmailExist = "email already registered";
	String responseDescriptionPhoneExist = "phone number already registered";
	String responseDescriptionUpdateSuccess = "user updated successfully";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TSC_006_UpdateProfile --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			getName = new File("./assets/generatedData/userName.txt");
			getPassword = new File("./assets/generatedData/userPassword.txt");
			getStaticEmail = new File("./assets/generatedData/userEmailStatic.txt");
			getStaticPhone = new File("./assets/generatedData/userPhoneStatic.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			passwordReader = new Scanner(getPassword);
			nameReader = new Scanner(getName);
			staticEmailReader = new Scanner(getStaticEmail);
			staticPhoneReader = new Scanner(getStaticPhone);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
			while (nameReader.hasNextLine()) {
				String data = nameReader.nextLine();
				existingUserName = data;
			}
			nameReader.close();
			
			while (passwordReader.hasNextLine()) {
				String data = passwordReader.nextLine();
				existingUserPassword = data;
			}
			passwordReader.close();
			
			while (staticPhoneReader.hasNextLine()) {
				String data = staticPhoneReader.nextLine();
				staticPhone = data;
			}
			staticPhoneReader.close();
			
			while (staticEmailReader.hasNextLine()) {
				String data = staticEmailReader.nextLine();
				staticEmail = data;
			}
			staticEmailReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Update data from specific existing user profile
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataUpdated", priority = 2)
	void TSC_006_TC001(String userPassword, String userEmail, String userPasswordUpdated, String userPhoneUpdated, String userNameUpdated, String userEmailUpdated) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_006_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("id", existingId);
		RequestParams.put("email", userEmailUpdated);
		RequestParams.put("password", userPasswordUpdated);
		RequestParams.put("phone", userPhoneUpdated);
		RequestParams.put("name", userNameUpdated);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.PUT, endpointUpdate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		
		logger.info("id to update : " + existingId);
		logger.info("=== Before update ===");
		logger.info("email : " + existingEmail);
		logger.info("password : " + existingUserPassword);
		logger.info("phone : " + existingPhone);
		logger.info("name : " + existingUserName);
		logger.info("=== Data for update ===");
		logger.info("email to update : " + userEmailUpdated);
		logger.info("password to update : " + userPasswordUpdated);
		logger.info("phone to update : " + userPhoneUpdated);
		logger.info("name to update : " + userNameUpdated);
		
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionUpdateSuccess);

		if (response.getStatusCode() == 200) {
			logger.info("Update data " + RequestParams  + " success !");
			try {
                FileWriter generateEmail = new FileWriter("assets/generatedData/userEmail.txt");
                FileWriter generatePhone = new FileWriter("assets/generatedData/userPhone.txt");
                FileWriter generateName = new FileWriter("assets/generatedData/userName.txt");
                FileWriter generatePassword = new FileWriter("assets/generatedData/userPassword.txt");
                generatePassword.write(userPasswordUpdated);
                generatePassword.close();
                generateEmail.write(userEmailUpdated);
                generateEmail.close();
                generatePhone.write(userPhoneUpdated);
                generatePhone.close();
                generateName.write(userNameUpdated);
                generateName.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
		}else {
			logger.info("Updated data " + RequestParams  + " failed !");
		}		
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_006_TC001 --- ]");
	}
	
	// Update data from specific existing user profile with existing phone number
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataAfterUpdatedExistingPhone", priority = 0)
	void TSC_006_TC002(String userPassword, String userEmail, String userPasswordUpdated, String userPhoneUpdated, String userNameUpdated, String userEmailUpdated) throws InterruptedException, ParseException {
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParams = new JSONObject();
		
		System.out.println(existingId);
		System.out.println(userEmailUpdated);
		System.out.println(userPasswordUpdated);
		System.out.println(userPhoneUpdated);
		System.out.println(userNameUpdated);
		
		RequestParams.put("id", existingId);
		RequestParams.put("email", userEmailUpdated);
		RequestParams.put("password", userPasswordUpdated);
		RequestParams.put("phone", userPhoneUpdated);
		RequestParams.put("name", userNameUpdated);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.PUT, endpointUpdate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		
		logger.info("id to update : " + existingId);
		logger.info("=== Before update ===");
		logger.info("email : " + existingEmail);
		logger.info("password : " + existingUserPassword);
		logger.info("phone : " + existingPhone);
		logger.info("name : " + existingUserName);
		logger.info("=== Data for update ===");
		logger.info("email to update : " + userEmailUpdated);
		logger.info("password to update : " + userPasswordUpdated);
		logger.info("phone to update : " + userPhoneUpdated);
		logger.info("name to update : " + userNameUpdated);
		
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionPhoneExist);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_006_TC002 --- ]");
	}
	
	// Update data from specific existing user profile with existing email
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataAfterUpdatedExistingEmail", priority = 1)
	void TSC_006_TC003(String userPassword, String userEmail, String userPasswordUpdated, String userPhoneUpdated, String userNameUpdated, String userEmailUpdated) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_006_TC003 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("id", existingId);
		RequestParams.put("email", userEmailUpdated);
		RequestParams.put("password", userPasswordUpdated);
		RequestParams.put("phone", userPhoneUpdated);
		RequestParams.put("name", userNameUpdated);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.PUT, endpointUpdate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		
		logger.info("id to update : " + existingId);
		logger.info("=== Before update ===");
		logger.info("email : " + existingEmail);
		logger.info("password : " + existingUserPassword);
		logger.info("phone : " + existingPhone);
		logger.info("name : " + existingUserName);
		logger.info("=== Data for update ===");
		logger.info("email to update : " + userEmailUpdated);
		logger.info("password to update : " + userPasswordUpdated);
		logger.info("phone to update : " + userPhoneUpdated);
		logger.info("name to update : " + userNameUpdated);
		
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionEmailExist);
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_006_TC003 --- ]");
	}
	
	@DataProvider(name="dataUpdated")
	Object[][] getDataBeforeUpdate() throws IOException{
		Object recordDataUser[][] = {{existingUserPassword.toString(), existingEmail.toString(), "Password@00Updated", randomPhoneNumber.toString(), "Updated"+existingUserName.toString(), "Updated"+existingEmail.toString()}};
		return(recordDataUser);
	}
	
	@DataProvider(name="dataAfterUpdatedExistingPhone")
	Object[][] getDataAfterUpdatePhoneExist() throws IOException{
		Object recordDataUser[][] = {{existingUserPassword.toString(), existingEmail.toString(), "Password@00UpdatedAgain", staticPhone.toString(), "UpdatedAgain"+existingUserName.toString(), "UpdatedAgain"+existingEmail.toString()}};
		return(recordDataUser);
	}
	
	@DataProvider(name="dataAfterUpdatedExistingEmail")
	Object[][] getDataAfterUpdateEmailExist() throws IOException{
		Object recordDataUser[][] = {{existingUserPassword.toString(), existingEmail.toString(), "Password@00UpdatedAgain", randomPhoneNumber.toString(), "UpdatedAgain"+existingUserName.toString(), staticEmail.toString()}};
		return(recordDataUser);
	}
	
	protected String getRandomNumber() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_006_UpdateProfile --- ]");
		System.out.println();
	}
}