package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_003_Login extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_003_Login.class);
	File getEmail, getPhone, getId, getOTP, getPassword;
	Scanner emailReader, phoneReader, idReader, otpReader, passwordReader;
	String baseUrl = "";
	String endpointTC01 = "/login";
	
	String existingEmail = "";
	String existingPhone = "";
	String otpToInput = "";
	String existingId = "";
	String existingPassword = "";
//	String responseDescriptionInvalidPassword = "login failed, wrong username or password";
	String responseDescriptionAccountNotExist = "login failed, the email/phone number or password did not match our records";
	String responseDescriptionInvalidPassword = "login failed, the email/phone number or password did not match our records";
	
	String responseStatusSuccess ="";
	String responseStatusFailed = "";

	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TSC_003_Login --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			getOTP = new File("./assets/generatedData/userOTP.txt");
			getPassword = new File("./assets/generatedData/userPassword.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			otpReader = new Scanner(getOTP);
			passwordReader = new Scanner(getPassword);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
			while (otpReader.hasNextLine()) {
				String data = otpReader.nextLine();
				otpToInput = data;
			}
			otpReader.close();
			
			while (passwordReader.hasNextLine()) {
				String data = passwordReader.nextLine();
				existingPassword = data;
			}
			passwordReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Check existing account with match email and password when loginStatus is False
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginExistEmailMatchPassword", priority = 0)
	void TSC_003_TC001(String userEmail, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_003_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("email", userEmail);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Login with email : " + userEmail);
		logger.info("Login with password : " + userPassword);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, "user logged in successfully");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_003_TC001 --- ]");
	}
	
	// Check existing account with match phone number and password when loginStatus is False
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginExistPhoneMatchPassword", priority = 1)
	void TSC_003_TC002(String userPhone, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_003_TC002 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Login with phone number : " + userPhone);
		logger.info("Login with password : " + userPassword);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, "user logged in successfully");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_003_TC002 --- ]");
	}
	
	// Check existing account with correct email and incorrect password
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginExistEmailRandomPassword", priority = 2)
	void TSC_003_TC003(String userEmail, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_003_TC003 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("email", userEmail);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionInvalidPassword);

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_003_TC003 --- ]");
	}
	
	// Check existing account with correct phone number and incorrect password
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginExistPhoneRandomPassword", priority = 3)
	void TSC_003_TC004(String userPhone, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_003_TC004 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionInvalidPassword);

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_003_TC004 --- ]");
	}
	
	// Check existing account with correct phone number and incorrect password
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginRandomEmailRandomPassword", priority = 4)
	void TSC_003_TC005(String userEmail, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_003_TC004 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("email", userEmail);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionAccountNotExist);

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_003_TC004 --- ]");
	}
	
	// Check account with incorrect phone number and incorrect password
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginRandomPhoneRandomPassword", priority = 5)
	void TSC_003_TC006(String userPhone, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_003_TC006 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionAccountNotExist);

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_003_TC006 --- ]");
	}
	
	@DataProvider(name="loginExistEmailMatchPassword")
	Object[][] getDataExistingEmail() throws IOException{
		Object recordDataUser[][] = {{existingEmail, existingPassword}};
		return(recordDataUser);
	}
	
	@DataProvider(name="loginExistPhoneMatchPassword")
	Object[][] getDataExistingPhone() throws IOException{
		Object recordDataUser[][] = {{existingPhone, existingPassword}};
		return(recordDataUser);
	}
	
	@DataProvider(name="loginExistEmailRandomPassword")
	Object[][] getDataExistingEmailRandomPassword() throws IOException{
		Object recordDataUser[][] = {{existingEmail, getRandomPassword()}};
		return(recordDataUser);
	}
	
	@DataProvider(name="loginExistPhoneRandomPassword")
	Object[][] getDataExistingPhoneRandomPassword() throws IOException{
		Object recordDataUser[][] = {{existingPhone, getRandomPassword()}};
		return(recordDataUser);
	}
	
	@DataProvider(name="loginRandomEmailRandomPassword")
	Object[][] getDataRandomEmailRandomPassword() throws IOException{
		Object recordDataUser[][] = {{"samplenotvalid@"+getRandomStringNumber()+".com", getRandomPassword()}};
		return(recordDataUser);
	}
	
	@DataProvider(name="loginRandomPhoneRandomPassword")
	Object[][] getDataRandomPhoneRandomPassword() throws IOException{
		Object recordDataUser[][] = {{"08120000"+getRandomPhone(), getRandomPassword()}};
		return(recordDataUser);
	}
	
	protected String getRandomPhone() {
        String SALTCHARS = "0123456789";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 4) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
	
	protected String getRandomPassword() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 4) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
	
	protected String getRandomStringNumber() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_003_Login --- ]");
		System.out.println();
	}
}