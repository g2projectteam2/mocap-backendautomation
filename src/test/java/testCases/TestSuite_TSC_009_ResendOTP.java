package testCases;

import org.testng.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_009_ResendOTP extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_009_ResendOTP.class);
	File getEmail, getPhone, getId, getName, getPassword, getStaticEmail, getStaticPhone;
	Scanner emailReader, phoneReader, idReader, nameReader, passwordReader, staticEmailReader, staticPhoneReader;
	String baseUrl = "";
	String endPointResendOTP = "/resendOTP";
	String responseDescriptionOTPsent = "com.twilio.exception.ApiException: The number  is unverified. Trial accounts cannot send messages to unverified numbers; verify  at twilio.com/user/account/phone-numbers/verified, or purchase a Twilio number to send messages to unverified numbers.";
//	String randomPhoneNumber = "+6285814759059";
//	String responseDescriptionOTPsent = "OTP sms sent";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String dataEmailStatic = "";
	String dataPhoneStatic = "";
	String existingEmail = "";
	String existingPhone = "";
	String existingId = "";
	String existingUserName = "";
	String existingUserPassword = "";
	String staticEmail = "";
	String staticPhone = "";
	String responseDescriptionUserNotExist = "user not found";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("\r\n");
		logger.info("[ --- Started TestSuite_TSC_008_Transaction --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			getName = new File("./assets/generatedData/userName.txt");
			getPassword = new File("./assets/generatedData/userPassword.txt");
			getStaticEmail = new File("./assets/generatedData/userEmailStatic.txt");
			getStaticPhone = new File("./assets/generatedData/userPhoneStatic.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			passwordReader = new Scanner(getPassword);
			nameReader = new Scanner(getName);
			staticEmailReader = new Scanner(getStaticEmail);
			staticPhoneReader = new Scanner(getStaticPhone);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
			while (nameReader.hasNextLine()) {
				String data = nameReader.nextLine();
				existingUserName = data;
			}
			nameReader.close();
			
			while (passwordReader.hasNextLine()) {
				String data = passwordReader.nextLine();
				existingUserPassword = data;
			}
			passwordReader.close();
			
			while (staticPhoneReader.hasNextLine()) {
				String data = staticPhoneReader.nextLine();
				staticPhone = data;
			}
			staticPhoneReader.close();
			
			while (staticEmailReader.hasNextLine()) {
				String data = staticEmailReader.nextLine();
				staticEmail = data;
			}
			staticEmailReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Resend otp when try to validate account
	@SuppressWarnings("unchecked")
	@Test(priority = 0)
	void TSC_009_TC001() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_009_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParams = new JSONObject();

		logger.info(" === Process insert balance === ");
		RequestParams.put("id", existingId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endPointResendOTP);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Resend OTP to user id : " + existingId);
		
		Assert.assertEquals(response.getStatusCode(), 200);	
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionOTPsent);
		
		if(response.getStatusCode() == 200) {
				logger.info(" === Process resend OTP success === ");
		}else {
			logger.info(" === Process resend OTP failed === ");
		}

		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_009_TC001 --- ]");
	}
	
	// Resend otp to not existing account
	@SuppressWarnings("unchecked")
	@Test(priority = 0)
	void TSC_009_TC002() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_009_TC002 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParams = new JSONObject();

		logger.info(" === Process insert balance === ");
		RequestParams.put("id", getRandomNumber());
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endPointResendOTP);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Resend OTP to user id : " + existingId);
		
		Assert.assertEquals(response.getStatusCode(), 404);	
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionUserNotExist);
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_009_TC002 --- ]");
	}
		
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_009_ResendOTP --- ]");
		System.out.println();
	}
	

}