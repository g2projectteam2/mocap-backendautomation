package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_004_Logout extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_004_Logout.class);
	File getEmail, getPhone, getId, getOTP;
	Scanner emailReader, phoneReader, idReader, otpReader;
	String baseUrl = "";
	String endpointTC01 = "/login";
	String endpointTC01Logout = "/logout";
	
	String existingEmail = "";
	String existingPhone = "";
	String otpToInput = "";
	String existingId = "";
	String responseDescriptionInvalidPassword = "login failed, wrong username or password";
	String responseDescriptionAccountNotExist = "login failed, user not exist";
	String responseDescriptionLogoutSuccess = "user logged out successfully";
	String responseDescriptionLoginSuccess = "user logged in successfully";
	
	String responseStatusSuccess ="";
	String responseStatusFailed = "";

	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TSC_004_Logout --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			getOTP = new File("./assets/generatedData/userOTP.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			otpReader = new Scanner(getOTP);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
			while (otpReader.hasNextLine()) {
				String data = otpReader.nextLine();
				otpToInput = data;
			}
			otpReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Check logout process with email login when loginStatus is True
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginExistEmailMatchPassword", priority = 0)
	void TSC_004_TC001(String userEmail, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_004_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("email", userEmail);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionLoginSuccess);
		
		JSONObject RequestParamsLogout = new JSONObject();
		
		RequestParamsLogout.put("id", existingId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsLogout.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01Logout);
		
		JSONParser parserLogout = new JSONParser();
		JSONObject jsonLogout = (JSONObject) parserLogout.parse(response.getBody().asString());
		String responseBodyLogout = response.getBody().asString();
		String responseStatusLogout = jsonLogout.get("status").toString();
		String responseDescriptionLogout = jsonLogout.get("description").toString();
		
		logger.info("Response body : " + responseBodyLogout);
		logger.info("Response status : " + responseStatusLogout);
		logger.info("Response Description : " + responseDescriptionLogout);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertTrue(responseBodyLogout != null);
		Assert.assertEquals(responseStatusLogout, responseStatusSuccess);
		Assert.assertEquals(responseDescriptionLogout, responseDescriptionLogoutSuccess);
		

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_004_TC001 --- ]");
	}
	
	// Check logout process with phone number login when loginStatus is True
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "loginExistPhoneMatchPassword", priority = 0)
	void TSC_004_TC002(String userPhone, String userPassword) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_004_TC002 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionLoginSuccess);
		
		JSONObject RequestParamsLogout = new JSONObject();
		
		RequestParamsLogout.put("id", existingId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsLogout.toJSONString());
		response = httpRequest.request(Method.POST, endpointTC01Logout);
		
		JSONParser parserLogout = new JSONParser();
		JSONObject jsonLogout = (JSONObject) parserLogout.parse(response.getBody().asString());
		String responseBodyLogout = response.getBody().asString();
		String responseStatusLogout = jsonLogout.get("status").toString();
		String responseDescriptionLogout = jsonLogout.get("description").toString();
		
		logger.info("Response body : " + responseBodyLogout);
		logger.info("Response status : " + responseStatusLogout);
		logger.info("Response Description : " + responseDescriptionLogout);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertTrue(responseBodyLogout != null);
		Assert.assertEquals(responseStatusLogout, responseStatusSuccess);
		Assert.assertEquals(responseDescriptionLogout, responseDescriptionLogoutSuccess);
		

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_004_TC002 --- ]");
	}
	
	@DataProvider(name="loginExistEmailMatchPassword")
	Object[][] getDataExistingEmail() throws IOException{
		Object recordDataUser[][] = {{existingEmail, "Password@00"}};
		return(recordDataUser);
	}
	
	@DataProvider(name="loginExistPhoneMatchPassword")
	Object[][] getDataExistingPhone() throws IOException{
		Object recordDataUser[][] = {{existingPhone, "Password@00"}};
		return(recordDataUser);
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_004_Logout --- ]");
		System.out.println();
	}
}