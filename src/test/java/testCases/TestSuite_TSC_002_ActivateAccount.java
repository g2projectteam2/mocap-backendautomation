
package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_002_ActivateAccount extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_002_ActivateAccount.class);
	File getEmail, getPhone, getId, getOTP;
	Scanner emailReader, phoneReader, idReader, otpReader;
	String baseUrl = "";
	String endpointActivate = "/register/activate";
	String endpontTC02 = "/all";
	String existingEmail = "";
	String existingPhone = "";
	String otpToInput = "";
	String existingId = "";
	String generatedRandomOTP = getRandomOTP();
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String responseDescriptionInvalidEMail = "invalid email address";
	String responseDescriptionInvalidPassword = "invalid password";
	String responseDescriptionEmailExist = "email already registered";
	String responseDescriptionPhoneExist = "phone number already registered";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TSC_002_ActivateAccount --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			getOTP = new File("./assets/generatedData/userOTP.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			otpReader = new Scanner(getOTP);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
			while (otpReader.hasNextLine()) {
				String data = otpReader.nextLine();
				otpToInput = data;
			}
			otpReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Activate registered account with id and match OTP
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "existingIdAccountWithValidOTP", priority = 0)
	void TSC_002_TC001(String UserId, String userOTP) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_002_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("id", UserId);
		RequestParams.put("otp", userOTP);
		logger.info("=== Process activate account ===");
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointActivate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Activate Account with id : " + UserId);
		logger.info("Activate Account with otp : " + userOTP);
		
		JSONObject tempDataOtp = new JSONObject();
		tempDataOtp = (JSONObject) json.get("user");
		String activeStatus = tempDataOtp.get("activeStatus").toString();
		
		
		logger.info("User activeStatus : "+activeStatus);
		Assert.assertEquals(activeStatus.toString(), "true");
		
		
		Assert.assertEquals(response.getStatusCode(), 200);
		if(response.getStatusCode() == 200) {
			logger.info("=== Process activate account success ===");
		}else {
			logger.info("=== Process activate account failed ===");
		}
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, "user registered successfully");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_002_TC001 --- ]");
	}
	
	// Activate registered account with email and match OTP
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "existingEmailAccountWithValidOTP", priority = 1)
	void TSC_002_TC002(String userEmail, String userOTP) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_002_TC002 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("email", userEmail);
		RequestParams.put("otp", userOTP);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointActivate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertTrue(response.getStatusCode() == 200);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, "user registered successfully");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_002_TC002 --- ]");
	}
	
	// Activate registered account with phone number and match OTP
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "existingPhoneAccountWithValidOTP", priority = 2)
	void TSC_002_TC003(String userPhone, String userOTP) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_002_TC003 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("otp", userOTP);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointActivate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertTrue(response.getStatusCode() == 200);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, "user registered successfully");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_002_TC003 --- ]");
	}
	
	// Check if input otp with otp in database is match
	@Test(priority = 3)
	@Parameters({"baseUrl"})
	void TSC_002_TC004(String baseUrl) throws InterruptedException, ParseException{
		logger.info("[ --- Started TSC_002_TC004 --- ]");
		String otpFromDB = "";
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			
			
			if(tempData.get("email").toString().equals(existingEmail) && tempData.get("phone").toString().equals(existingPhone) && tempData.get("id").toString().equals(existingId) ) {
				otpFromDB = tempData.get("otp").toString();
				logger.info("Data user from Database : " + tempData);
				logger.info("OTP to input : " + otpToInput);
				logger.info("OTP from DataBase : " + otpFromDB);
			};
		}
		Assert.assertEquals(otpToInput, otpFromDB);;
		logger.info("[ --- Finished TSC_002_TC004 --- ]");
		Thread.sleep(3);
	}
	
	// Check and make sure the user account is logged in
	@Test(priority = 4)
	@Parameters({"baseUrl"})
	void TSC_002_TC005(String baseUrl) throws InterruptedException, ParseException{
		logger.info("[ --- Started TSC_002_TC005 --- ]");
		String loginStatus = "";
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			
			
			if(tempData.get("email").toString().equals(existingEmail) && tempData.get("phone").toString().equals(existingPhone) && tempData.get("id").toString().equals(existingId) ) {
				loginStatus = tempData.get("loginStatus").toString();
				logger.info("Data user from Database : " + tempData);
			};
		}
		Assert.assertEquals(loginStatus, "true");
		logger.info("[ --- Finished TSC_002_TC005 --- ]");
		Thread.sleep(3);
	}
	
	// Check and make sure the user account is active
	@Test(priority = 5)
	@Parameters({"baseUrl"})
	void TSC_002_TC006(String baseUrl) throws InterruptedException, ParseException{
		logger.info("[ --- Started TSC_002_TC006 --- ]");
		String activeStatus = "";
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			
			
			if(tempData.get("email").toString().equals(existingEmail) && tempData.get("phone").toString().equals(existingPhone) && tempData.get("id").toString().equals(existingId) ) {
				activeStatus = tempData.get("activeStatus").toString();
				logger.info("Data user from Database : " + tempData);
			};
		}
		Assert.assertEquals(activeStatus, "true");
		logger.info("[ --- Finished TSC_002_TC006 --- ]");
		Thread.sleep(3);
	}

	// Activate registered account with id and not match OTP
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "existingIdAccountWithInvalidOTP", priority = 6)
	void TSC_002_TC007(String UserId, String userOTP) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_002_TC007 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("id", UserId);
		RequestParams.put("otp", userOTP);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointActivate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, "OTP does not match, please try again");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_002_TC007 --- ]");
	}
	
	// Activate registered account with email and not match OTP
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "existingEmailAccountWithInvalidOTP", priority = 7)
	void TSC_002_TC008(String userEmail, String userOTP) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_002_TC008 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("email", userEmail);
		RequestParams.put("otp", userOTP);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointActivate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, "OTP does not match, please try again");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_002_TC008 --- ]");
	}
	
	// Activate registered account with phone and not match OTP
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "existingPhoneAccountWithInvalidOTP", priority = 8)
	void TSC_002_TC009(String userPhone, String userOTP) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_002_TC009 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("otp", userOTP);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointActivate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, "OTP does not match, please try again");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_002_TC009 --- ]");
	}
	
	// Check if account is not exist
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "notExistingEmailAccountWithInvalidOTP", priority = 9)
	void TSC_002_TC010(String userEmail, String userOTP) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_002_TC010 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("email", userEmail);
		RequestParams.put("otp", userOTP);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointActivate);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, "user not found");

		Thread.sleep(5);
		logger.info("[ --- Finished TSC_002_TC010 --- ]");
	}
	
	@DataProvider(name="existingIdAccountWithValidOTP")
	Object[][] getDataExistingId() throws IOException{
		Object recordDataUser[][] = {{existingId, otpToInput}};
		return(recordDataUser);
	}
	
	@DataProvider(name="existingPhoneAccountWithValidOTP")
	Object[][] getDataExistingPhone() throws IOException{
		Object recordDataUser[][] = {{existingPhone, otpToInput}};
		return(recordDataUser);
	}
	
	@DataProvider(name="existingEmailAccountWithValidOTP")
	Object[][] getDataExistingEmail() throws IOException{
		Object recordDataUser[][] = {{existingEmail, otpToInput}};
		return(recordDataUser);
	}
	
	@DataProvider(name="existingIdAccountWithInvalidOTP")
	Object[][] getDataExistingIdError() throws IOException{
		Object recordDataUser[][] = {{existingId, generatedRandomOTP}};
		return(recordDataUser);
	}
	
	@DataProvider(name="existingPhoneAccountWithInvalidOTP")
	Object[][] getDataExistingPhoneError() throws IOException{
		Object recordDataUser[][] = {{existingPhone, generatedRandomOTP}};
		return(recordDataUser);
	}
	
	@DataProvider(name="existingEmailAccountWithInvalidOTP")
	Object[][] getDataExistingEmailError() throws IOException{
		Object recordDataUser[][] = {{existingEmail, generatedRandomOTP}};
		return(recordDataUser);
	}
	
	@DataProvider(name="notExistingEmailAccountWithInvalidOTP")
	Object[][] getDataNotExistingEmailError() throws IOException{
		Object recordDataUser[][] = {{"notexist@domain.com", generatedRandomOTP}};
		return(recordDataUser);
	}
	
	protected String getRandomOTP() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 6) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_002_ActivateAccount --- ]");
		System.out.println();
	}
}