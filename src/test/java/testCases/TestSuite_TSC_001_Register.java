package testCases;

import java.io.IOException;

import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import base.TestBase;

public class TestSuite_TSC_001_Register extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_001_Register.class);
	
	File getStaticPhone, getStaticEmail;
	Scanner emailStaticReader, phoneStaticReader;
	String baseUrl = "";
	String endpointRegister = "/register/validate";
	String endpontTC02 = "/all";
	String randStringNumber = getRandomStringNumber();
	String randomEmail = "Vijay"+randStringNumber+"@gmail.com";
	String randomName = "Vijay " + randStringNumber;
	String password = "Password@00";
	String randomPhoneNumber = getRandomPhone();
	String responseDescriptionOTPsent = "com.twilio.exception.ApiException: The number  is unverified. Trial accounts cannot send messages to unverified numbers; verify  at twilio.com/user/account/phone-numbers/verified, or purchase a Twilio number to send messages to unverified numbers.";
//	String randomPhoneNumber = "+6285814759059";
//	String responseDescriptionOTPsent = "OTP sms sent";
	String invalidRandomEmail = "Vijay"+randStringNumber+".com";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String responseDescriptionInvalidEMail = "invalid email address";
	String responseDescriptionInvalidPassword = "invalid password";
	String responseDescriptionEmailExist = "email already registered";
	String responseDescriptionPhoneExist = "phone number already registered";
	
	String invalidPassword = "pwd";
	String responseStatusAfterSend = "";
	String generatedOTP = "";
	String OTPfromDataBase = "";
	String dataEmailStatic = "";
	String dataPhoneStatic = "";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TSC_001_Register --- ]");
	}
	
	// Send all required data with valid format to server 
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataUser")
	void TSC_001_TC001(String userPassword, String userPhone, String userName, String userEmail) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_001_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("password", userPassword);
		RequestParams.put("phone", userPhone);
		RequestParams.put("name", userName);
		RequestParams.put("email", userEmail);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		
		
		Assert.assertEquals(response.getStatusCode(), 201);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionOTPsent);
		if(responseDescription.toString().equals(responseDescriptionOTPsent)) {
			responseStatusAfterSend = "OTP sms sent";
			JSONObject tempDataOtp = new JSONObject();
			tempDataOtp = (JSONObject) json.get("user");
			generatedOTP = tempDataOtp.get("otp").toString();
		}
		
		if (response.getStatusCode() == 201) {
			logger.info("Create data " + RequestParams  + " success !");
			try {
                FileWriter generateEmail = new FileWriter("assets/generatedData/userEmail.txt");
                FileWriter generatePhone = new FileWriter("assets/generatedData/userPhone.txt");
                FileWriter generateName = new FileWriter("assets/generatedData/userName.txt");
                FileWriter generatePassword = new FileWriter("assets/generatedData/userPassword.txt");
                generatePassword.write(userPassword);
                generatePassword.close();
                generateEmail.write(userEmail);
                generateEmail.close();
                generatePhone.write(userPhone);
                generatePhone.close();              
                generateName.write(userName);
                generateName.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			
			try {
				getStaticPhone = new File("./assets/generatedData/userPhoneStatic.txt");
				getStaticEmail = new File("./assets/generatedData/userEmailStatic.txt");
				emailStaticReader = new Scanner(getStaticEmail);
				phoneStaticReader = new Scanner(getStaticPhone);
				while (emailStaticReader.hasNextLine()) {
					String data = emailStaticReader.nextLine();
					dataEmailStatic = data;
				}
				emailStaticReader.close();
				
				while (phoneStaticReader.hasNextLine()) {
					String data = phoneStaticReader.nextLine();
					dataPhoneStatic = data;
				}
				phoneStaticReader.close();
				
			} catch (FileNotFoundException e) {
				System.out.println("An error occurred.");
			      e.printStackTrace();
			}
			
			if (dataEmailStatic.equals("")) {
				try {
	                FileWriter generateEmailStatic = new FileWriter("assets/generatedData/userEmailStatic.txt");
	                generateEmailStatic.write(userEmail);
	                generateEmailStatic.close();
	            } catch (IOException e) {
	                System.err.format("IOException: %s%n", e);
	            }
			}
			
			if (dataPhoneStatic.equals("")) {
				try {
	                FileWriter generatePhoneStatic = new FileWriter("assets/generatedData/userPhoneStatic.txt");
	                generatePhoneStatic.write(userPhone);
	                generatePhoneStatic.close();
	            } catch (IOException e) {
	                System.err.format("IOException: %s%n", e);
	            }
			}
	
			response = httpRequest.request(Method.GET, endpontTC02);
					
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONArray jsonarray = new JSONArray();
			jsonarray = (JSONArray) json.get("users");
			
			for(int i = 0; i <=jsonarray.size()-1; i++) {
				JSONObject tempData = new JSONObject();
				tempData = (JSONObject) jsonarray.get(i);
				
				
				if(tempData.get("email").toString().equals(userEmail) && tempData.get("phone").toString().equals(userPhone) && tempData.get("name").toString().equals(userName)  ) {
					logger.info("Data user from Database : " + tempData);
					try {
		                FileWriter generateId = new FileWriter("assets/generatedData/userId.txt");
		                FileWriter generateOTP = new FileWriter("assets/generatedData/userOTP.txt");
		                generateId.write(tempData.get("id").toString());
		                generateId.close();
		                generateOTP.write(tempData.get("otp").toString());
		                generateOTP.close();
		            } catch (IOException e) {
		                System.err.format("IOException: %s%n", e);
		            }
				};
			}
			
		}else {
			logger.info("Create data " + RequestParams  + " failed !");
		}		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_001_TC001 --- ]");
	}
	
	// Check if data is in the database
	@Test
	@Parameters({"baseUrl"})
	void TSC_001_TC002(String baseUrl) throws InterruptedException, ParseException{
		logger.info("[ --- Started TSC_001_TC002 --- ]");
		int testStatus = 0;
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			
			
			if(tempData.get("email").toString().equals(randomEmail) && tempData.get("phone").toString().equals(randomPhoneNumber) ) {
				logger.info("Data user from Database : " + tempData);
				testStatus = 1;
			};
		}
		Assert.assertTrue(testStatus == 1);
		logger.info("[ --- Finished TSC_001_TC002 --- ]");
		Thread.sleep(3);
	}
	
	// Check if the OTP is sent
	@Test
	void TSC_001_TC003() {
		logger.info("[ --- Started TSC_001_TC003 --- ]");
		Assert.assertEquals(responseStatusAfterSend, "OTP sms sent");
		logger.info("Response status : " + responseStatusAfterSend);
		logger.info("[ --- Finished TSC_001_TC003 --- ]");
	}
	
	// Check if the field OTP in database is filled
	@Test
	void TSC_001_TC004() throws ParseException{
		logger.info("[ --- Started TSC_001_TC004 --- ]");
		boolean isOTPfilled = false;
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			if(tempData.get("email").toString().equals(randomEmail) && tempData.get("phone").toString().equals(randomPhoneNumber) ) {
				if(tempData.get("otp") != null) {
					isOTPfilled = true;
				}
				OTPfromDataBase = tempData.get("otp").toString();
				logger.info("OTP : "+tempData.get("otp"));
			};
		}
		
		Assert.assertEquals(isOTPfilled, true);
		logger.info("[ --- Finished TSC_001_TC004 --- ]");
		
	}
	
	// Check consistency of sent OTP with OTP in database
	@Test
	void TSC_001_TC005() {
		logger.info("[ --- Started TSC_001_TC005 --- ]");
		Assert.assertEquals(generatedOTP, "[sent]");
		logger.info("[ --- Finished TSC_001_TC005 --- ]");
	}
	
	// Check and make sure the user account isn't active
	@Test
	void TSC_001_TC006() throws ParseException{
		logger.info("[ --- Started TSC_001_TC006 --- ]");
		
		boolean isUserActive = false;
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			if(tempData.get("email").toString().equals(randomEmail) && tempData.get("phone").toString().equals(randomPhoneNumber) ) {
				if(tempData.get("activeStatus").toString() == "false") {
					isUserActive = false;
				}else {
					isUserActive = true;
				}
				OTPfromDataBase = tempData.get("otp").toString();
				logger.info("Active Status : "+tempData.get("activeStatus"));
			};
		}
		Assert.assertEquals(isUserActive, false);
		logger.info("[ --- Finished TSC_001_TC006 --- ]");
	}
	
	// Check and make sure the user account is not logged in
	@Test
	void TSC_001_TC007() throws ParseException{
		logger.info("[ --- Started TSC_001_TC007 --- ]");
		boolean isLoggedIn = false;
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			if(tempData.get("email").toString().equals(randomEmail) && tempData.get("phone").toString().equals(randomPhoneNumber) ) {
				if(tempData.get("loginStatus").toString() == "false") {
					isLoggedIn = false;
				}else {
					isLoggedIn = true;
				}
				OTPfromDataBase = tempData.get("otp").toString();
				logger.info("Login Status : "+tempData.get("loginStatus"));
			};
		}
		Assert.assertEquals(isLoggedIn, false);
		logger.info("[ --- Finished TSC_001_TC007 --- ]");
	}
	
	// Check and make sure the user balance is empty
	@Test
	void TSC_001_TC008() throws ParseException{
		logger.info("[ --- Started TSC_001_TC008 --- ]");
		int userBalance = 0;
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpontTC02);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("users");
		
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);
			if(tempData.get("email").toString().equals(randomEmail) && tempData.get("phone").toString().equals(randomPhoneNumber) ) {
				if(Integer.parseInt(tempData.get("balance").toString())  == 0) {
					userBalance = 0;
				}else {
					userBalance = Integer.parseInt(tempData.get("balance").toString());
				}
				OTPfromDataBase = tempData.get("otp").toString();
				logger.info("User Balance : "+Integer.parseInt(tempData.get("balance").toString()));
			};
		}
		Assert.assertEquals(userBalance, 0);
		logger.info("[ --- Finished TSC_001_TC008 --- ]");
	}
	
	// Send all required data with invalid email format to server
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "invalidDataEmailUser")
	void TSC_001_TC009(String userPassword, String userPhone, String userName, String userEmail) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_001_TC009 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("password", userPassword);
		RequestParams.put("phone", userPhone);
		RequestParams.put("name", userName);
		RequestParams.put("email", userEmail);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + response.getStatusCode());
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertTrue(response.getStatusCode() == 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionInvalidEMail);
		
		logger.info("[ --- Finished TSC_001_TC009 --- ]");
	}
	
	// Send all required data with invalid password format to server
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "invalidDataPasswordUser")
	void TSC_001_TC010(String userPassword, String userPhone, String userName, String userEmail) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_001_TC010 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("password", userPassword);
		RequestParams.put("phone", userPhone);
		RequestParams.put("name", userName);
		RequestParams.put("email", userEmail);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + response.getStatusCode());
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertTrue(response.getStatusCode() == 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionInvalidPassword);
		logger.info("[ --- Finished TSC_001_TC010 --- ]");
	}
	
	// Send data with existing email
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataUserWithExistingEmail")
	void TSC_001_TC011(String userPassword, String userPhone, String userName, String userEmail) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_001_TC011 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("password", userPassword);
		RequestParams.put("phone", userPhone);
		RequestParams.put("name", userName);
		RequestParams.put("email", userEmail);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + response.getStatusCode());
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertTrue(response.getStatusCode() == 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionEmailExist);
		logger.info("[ --- Finished TSC_001_TC011 --- ]");
	}

	// Send data with existing phone number
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataUserWithExistingPhone")
	void TSC_001_TC012(String userPassword, String userPhone, String userName, String userEmail) throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_001_TC012 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("password", userPassword);
		RequestParams.put("phone", userPhone);
		RequestParams.put("name", userName);
		RequestParams.put("email", userEmail);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + response.getStatusCode());
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertTrue(response.getStatusCode() == 400);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionPhoneExist);
		logger.info("[ --- Finished TSC_001_TC012 --- ]");
	}
	
	@DataProvider(name="dataUser")
	Object[][] getDataUser() throws IOException{
		Object recordDataUser[][] = {{password, randomPhoneNumber, randomName, randomEmail}};
		return(recordDataUser);
	}
	
	@DataProvider(name="dataUserWithExistingEmail")
	Object[][] getDataExistingEmail() throws IOException{
		Object recordDataUser[][] = {{password, "0812"+getRandomNumber(), randomName, randomEmail}};
		return(recordDataUser);
	}
	
	@DataProvider(name="dataUserWithExistingPhone")
	Object[][] getDataExistingPhone() throws IOException{
		Object recordDataUser[][] = {{password, randomPhoneNumber, randomName, "Vijay"+getRandomStringNumber()+"@gmail.com"}};
		return(recordDataUser);
	}
	
	@DataProvider(name="invalidDataEmailUser")
	Object[][] getInvalidDataEmail() throws IOException{
		Object recordDataUser[][] = {{password, randomPhoneNumber, randomName, invalidRandomEmail}};
		return(recordDataUser);
	}
	
	@DataProvider(name="invalidDataPasswordUser")
	Object[][] getInvalidDataPassword() throws IOException{
		Object recordDataUser[][] = {{invalidPassword, randomPhoneNumber, randomName, randomEmail}};
		return(recordDataUser);
	}
	
	protected String getRandomStringNumber() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

	protected String getRandomNumber() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_001_Register --- ]");
		System.out.println();
	}
}