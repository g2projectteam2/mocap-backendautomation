package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_007_ForgotPassword extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_007_ForgotPassword.class);
	File getEmail, getPhone, getId, getName, getPassword, getStaticEmail, getStaticPhone;
	Scanner emailReader, phoneReader, idReader, nameReader, passwordReader, staticEmailReader, staticPhoneReader;
	String baseUrl = "";
	String endpointRequest = "/forgotPassword/request";
	String endpointValidate = "/forgotPassword/validate";
	String endpointUpdate = "/updatePassword";
	String endPointGetAllUser = "/all";
	
	String existingEmail = "";
	String existingPhone = "";
	String existingId = "";
	String existingUserName = "";
	String existingUserPassword = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String staticEmail = "";
	String staticPhone = "";
	String newPassword = "NewPassword@"+getRandomNumber();
	String randomOTP = generateRandomOTP();
	
	String randomPhoneNumber = getRandomPhone();
	String responseDescriptionOTPsent = "com.twilio.exception.ApiException: The number  is unverified. Trial accounts cannot send messages to unverified numbers; verify  at twilio.com/user/account/phone-numbers/verified, or purchase a Twilio number to send messages to unverified numbers.";
//	String randomPhoneNumber = "+6285814759059";
//	String responseDescriptionOTPsent = "OTP sms sent";
	
	
	String randomEmail = "Ujay"+generateRandomStringNumberForEmail()+"@random.com";

	String responseOTPValid = "OTP valid";
	String responseOTPInvalid = "OTP invalid";
	String responseDescriptionInvalidEMail = "invalid email address";
	String responseDescriptionInvalidPassword = "invalid password";
	String responseDescriptionAccountNotExist = "user not found";

	
	String responseDescriptionUpdateSuccess = "password updated successfully";
	
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TSC_007_ForgotPassword --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			getName = new File("./assets/generatedData/userName.txt");
			getPassword = new File("./assets/generatedData/userPassword.txt");
			getStaticEmail = new File("./assets/generatedData/userEmailStatic.txt");
			getStaticPhone = new File("./assets/generatedData/userPhoneStatic.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			passwordReader = new Scanner(getPassword);
			nameReader = new Scanner(getName);
			staticEmailReader = new Scanner(getStaticEmail);
			staticPhoneReader = new Scanner(getStaticPhone);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
			while (nameReader.hasNextLine()) {
				String data = nameReader.nextLine();
				existingUserName = data;
			}
			nameReader.close();
			
			while (passwordReader.hasNextLine()) {
				String data = passwordReader.nextLine();
				existingUserPassword = data;
			}
			passwordReader.close();
			
			while (staticPhoneReader.hasNextLine()) {
				String data = staticPhoneReader.nextLine();
				staticPhone = data;
			}
			staticPhoneReader.close();
			
			while (staticEmailReader.hasNextLine()) {
				String data = staticEmailReader.nextLine();
				staticEmail = data;
			}
			staticEmailReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Forgot Password and reset to new password with email
	@SuppressWarnings("unchecked")
	@Test(priority = 0)
	void TSC_007_TC001() throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_007_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParamsReq = new JSONObject();
		
		// REQUEST FORGOT PASSWORD BY EMAIL
		logger.info(" === Process request for reset password === ");
		RequestParamsReq.put("email", existingEmail);

		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsReq.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequest);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		
		logger.info("updated password for email : " + existingEmail);
		
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionOTPsent);

		// IF REQUEST SUCCESS
		if (response.getStatusCode() == 200) {
			logger.info("Request for reset password data " + RequestParamsReq  + " success !");
			
			// SEND OTP TO FILE TXT AND VALIDATE THE OTP
			response = httpRequest.request(Method.GET, endPointGetAllUser);
			
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONArray jsonarray = new JSONArray();
			jsonarray = (JSONArray) json.get("users");
			
			for(int i = 0; i <=jsonarray.size()-1; i++) {
				JSONObject tempData = new JSONObject();
				tempData = (JSONObject) jsonarray.get(i);
				if(tempData.get("email").toString().equals(existingEmail)) {
					
					
					// VALIDATE OTP
					logger.info(" === Process validate otp === ");
					JSONObject RequestParamsValidate = new JSONObject();
					RequestParamsValidate = new JSONObject();
					RequestParamsValidate.put("id", tempData.get("id").toString());
					RequestParamsValidate.put("otp", tempData.get("otp").toString());
					
					httpRequest.header("Content-Type", "application/json");
					httpRequest.body(RequestParamsValidate.toJSONString());		
					response = httpRequest.request(Method.POST, endpointValidate);
					parser = new JSONParser();
					json = (JSONObject) parser.parse(response.getBody().asString());
					responseBody = response.getBody().asString();
					responseStatus = json.get("status").toString();
					responseDescription = json.get("description").toString();
					
					logger.info("Response body : " + responseBody);
					logger.info("Response status : " + responseStatus);
					logger.info("Response Description : " + responseDescription);
					Assert.assertTrue(responseBody != null);
					Assert.assertEquals(responseStatus, responseStatusSuccess);
					Assert.assertEquals(responseDescription, responseOTPValid);
					
					// IF VALID, UPDATE PASSWORD
					if(responseDescription.toString().equals(responseOTPValid.toString())) {
						logger.info(" === Proses validate otp : success === ");
						
						logger.info(" === Process update password === ");
						
						
						JSONObject RequestParamsUpdate = new JSONObject();
						RequestParamsUpdate = new JSONObject();
						RequestParamsUpdate.put("id", tempData.get("id").toString());
						RequestParamsUpdate.put("password", newPassword);
						
						httpRequest.header("Content-Type", "application/json");
						httpRequest.body(RequestParamsUpdate.toJSONString());		
						response = httpRequest.request(Method.POST, endpointUpdate);
						parser = new JSONParser();
						json = (JSONObject) parser.parse(response.getBody().asString());
						responseBody = response.getBody().asString();
						responseStatus = json.get("status").toString();
						responseDescription = json.get("description").toString();
						
						logger.info("Response body : " + responseBody);
						logger.info("Response status : " + responseStatus);
						logger.info("Response Description : " + responseDescription);
						logger.info("Updated password for ID : " + tempData.get("id"));
						logger.info("Updated password for email : " + existingEmail);
						logger.info("New Password : " + newPassword);
						Assert.assertTrue(responseBody != null);
						Assert.assertEquals(responseStatus, responseStatusSuccess);
						Assert.assertEquals(responseDescription, responseDescriptionUpdateSuccess);
						
						// SAVE OTP TO FILE TXT
						try {
			                FileWriter generatePassword = new FileWriter("assets/generatedData/userPassword.txt");
			                generatePassword.write(newPassword);
			                generatePassword.close();
			            } catch (IOException e) {
			                System.err.format("IOException: %s%n", e);
			            }
						
					}else {
						Assert.assertTrue(false);
						logger.info(" === Proses validate otp : failed === ");
					}
					
					
					// SAVE OTP TO FILE TXT
					try {
		                FileWriter generateId = new FileWriter("assets/generatedData/userId.txt");
		                FileWriter generateOTP = new FileWriter("assets/generatedData/userOTP.txt");
		                generateId.write(tempData.get("id").toString());
		                generateId.close();
		                generateOTP.write(tempData.get("otp").toString());
		                generateOTP.close();
		            } catch (IOException e) {
		                System.err.format("IOException: %s%n", e);
		            }
				};
			}
			
		}else {
			Assert.assertTrue(false);
			logger.info("Request for reset password data " + RequestParamsReq  + " failed !");
		}		
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_007_TC001 --- ]");
	}
	
	// Forgot Password and reset to new password with phone number
	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	void TSC_007_TC002() throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_007_TC002 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParamsReq = new JSONObject();
		
		// REQUEST FORGOT PASSWORD BY PHONE NUMBER
		logger.info(" === Process request for reset password === ");
		RequestParamsReq.put("phone", existingPhone);

		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsReq.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequest);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		
		logger.info("updated password for email : " + existingEmail);
		
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionOTPsent);

		// IF REQUEST SUCCESS
		if (response.getStatusCode() == 200) {
			logger.info("Request for reset password data " + RequestParamsReq  + " success !");
			
			// SEND OTP TO FILE TXT AND VALIDATE THE OTP
			response = httpRequest.request(Method.GET, endPointGetAllUser);
			
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONArray jsonarray = new JSONArray();
			jsonarray = (JSONArray) json.get("users");
			
			for(int i = 0; i <=jsonarray.size()-1; i++) {
				JSONObject tempData = new JSONObject();
				tempData = (JSONObject) jsonarray.get(i);
				if(tempData.get("email").toString().equals(existingEmail)) {
					
					
					// VALIDATE OTP
					logger.info(" === Process validate otp === ");
					JSONObject RequestParamsValidate = new JSONObject();
					RequestParamsValidate = new JSONObject();
					RequestParamsValidate.put("id", tempData.get("id").toString());
					RequestParamsValidate.put("otp", tempData.get("otp").toString());
					
					httpRequest.header("Content-Type", "application/json");
					httpRequest.body(RequestParamsValidate.toJSONString());		
					response = httpRequest.request(Method.POST, endpointValidate);
					parser = new JSONParser();
					json = (JSONObject) parser.parse(response.getBody().asString());
					responseBody = response.getBody().asString();
					responseStatus = json.get("status").toString();
					responseDescription = json.get("description").toString();
					
					logger.info("Response body : " + responseBody);
					logger.info("Response status : " + responseStatus);
					logger.info("Response Description : " + responseDescription);
					Assert.assertTrue(responseBody != null);
					Assert.assertEquals(responseStatus, responseStatusSuccess);
					Assert.assertEquals(responseDescription, responseOTPValid);
					
					// IF VALID, UPDATE PASSWORD
					if(responseDescription.toString().equals(responseOTPValid.toString())) {
						logger.info(" === Proses validate otp : success === ");
						
						logger.info(" === Process update password === ");
						
						
						JSONObject RequestParamsUpdate = new JSONObject();
						RequestParamsUpdate = new JSONObject();
						RequestParamsUpdate.put("id", tempData.get("id").toString());
						RequestParamsUpdate.put("password", newPassword);
						
						httpRequest.header("Content-Type", "application/json");
						httpRequest.body(RequestParamsUpdate.toJSONString());		
						response = httpRequest.request(Method.POST, endpointUpdate);
						parser = new JSONParser();
						json = (JSONObject) parser.parse(response.getBody().asString());
						responseBody = response.getBody().asString();
						responseStatus = json.get("status").toString();
						responseDescription = json.get("description").toString();
						
						logger.info("Response body : " + responseBody);
						logger.info("Response status : " + responseStatus);
						logger.info("Response Description : " + responseDescription);
						logger.info("Updated password for ID : " + tempData.get("id"));
						logger.info("Updated password for email : " + existingEmail);
						logger.info("New Password : " + newPassword);
						Assert.assertTrue(responseBody != null);
						Assert.assertEquals(responseStatus, responseStatusSuccess);
						Assert.assertEquals(responseDescription, responseDescriptionUpdateSuccess);
						
						// SAVE OTP TO FILE TXT
						try {
			                FileWriter generatePassword = new FileWriter("assets/generatedData/userPassword.txt");
			                generatePassword.write(newPassword);
			                generatePassword.close();
			            } catch (IOException e) {
			                System.err.format("IOException: %s%n", e);
			            }
						
					}else {
						Assert.assertTrue(false);
						logger.info(" === Proses validate otp : failed === ");
					}
					
					
					// SAVE OTP TO FILE TXT
					try {
		                FileWriter generateId = new FileWriter("assets/generatedData/userId.txt");
		                FileWriter generateOTP = new FileWriter("assets/generatedData/userOTP.txt");
		                generateId.write(tempData.get("id").toString());
		                generateId.close();
		                generateOTP.write(tempData.get("otp").toString());
		                generateOTP.close();
		            } catch (IOException e) {
		                System.err.format("IOException: %s%n", e);
		            }
				};
			}
			
		}else {
			Assert.assertTrue(false);
			logger.info("Request for reset password data " + RequestParamsReq  + " failed !");
		}		
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_007_TC002 --- ]");
	}
	
	// Reset new password by email with invalid otp
	@SuppressWarnings("unchecked")
	@Test(priority = 2)
	void TSC_007_TC003() throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_007_TC003 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParamsReq = new JSONObject();
		
		// REQUEST FORGOT PASSWORD BY EMAIL
		logger.info(" === Process request for reset password === ");
		RequestParamsReq.put("email", existingEmail);

		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsReq.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequest);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		
		logger.info("updated password for email : " + existingEmail);
		
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionOTPsent);

		// IF REQUEST SUCCESS
		if (response.getStatusCode() == 200) {
			logger.info("Request for reset password data " + RequestParamsReq  + " success !");
			
			// SEND OTP TO FILE TXT AND VALIDATE THE OTP
			response = httpRequest.request(Method.GET, endPointGetAllUser);
			
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONArray jsonarray = new JSONArray();
			jsonarray = (JSONArray) json.get("users");
			
			for(int i = 0; i <=jsonarray.size()-1; i++) {
				JSONObject tempData = new JSONObject();
				tempData = (JSONObject) jsonarray.get(i);
				if(tempData.get("email").toString().equals(existingEmail)) {					
					// VALIDATE OTP
					logger.info(" === Process validate otp === ");
					JSONObject RequestParamsValidate = new JSONObject();
					RequestParamsValidate = new JSONObject();
					RequestParamsValidate.put("id", tempData.get("id").toString());
					RequestParamsValidate.put("otp", randomOTP);
					
					httpRequest.header("Content-Type", "application/json");
					httpRequest.body(RequestParamsValidate.toJSONString());		
					response = httpRequest.request(Method.POST, endpointValidate);
					parser = new JSONParser();
					json = (JSONObject) parser.parse(response.getBody().asString());
					responseBody = response.getBody().asString();
					responseStatus = json.get("status").toString();
					responseDescription = json.get("description").toString();
					
					logger.info("Response body : " + responseBody);
					logger.info("Response status : " + responseStatus);
					logger.info("Response Description : " + responseDescription);
					logger.info("Updated password for ID : " + tempData.get("id"));
					logger.info("Updated password for email : " + existingEmail);
					logger.info("Actual OTP : " + tempData.get("otp"));
					logger.info("Input OTP : " + randomOTP);
					
					
					
					Assert.assertTrue(responseBody != null);
					Assert.assertEquals(responseStatus, responseStatusFailed);
					Assert.assertEquals(responseDescription, responseOTPInvalid);
					Assert.assertEquals(response.getStatusCode(), 400);
					
				};
			}
			
		}else {
			Assert.assertTrue(false);
			logger.info("Request for reset password data " + RequestParamsReq  + " failed !");
		}		
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_007_TC003 --- ]");
	}
	
	// Reset new password by phone number with invalid otp
	@SuppressWarnings("unchecked")
	@Test(priority = 3)
	void TSC_007_TC004() throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_007_TC004 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParamsReq = new JSONObject();
		
		// REQUEST FORGOT PASSWORD BY PHONE NUMBER
		logger.info(" === Process request for reset password === ");
		RequestParamsReq.put("phone", existingPhone);

		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsReq.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequest);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		Assert.assertEquals(response.getStatusCode(), 200);
		
		logger.info("updated password for email : " + existingEmail);
		
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionOTPsent);

		// IF REQUEST SUCCESS
		if (response.getStatusCode() == 200) {
			logger.info("Request for reset password data " + RequestParamsReq  + " success !");
			
			// SEND OTP TO FILE TXT AND VALIDATE THE OTP
			response = httpRequest.request(Method.GET, endPointGetAllUser);
			
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONArray jsonarray = new JSONArray();
			jsonarray = (JSONArray) json.get("users");
			
			for(int i = 0; i <=jsonarray.size()-1; i++) {
				JSONObject tempData = new JSONObject();
				tempData = (JSONObject) jsonarray.get(i);
				if(tempData.get("email").toString().equals(existingEmail)) {
					// VALIDATE OTP
					logger.info(" === Process validate otp === ");
					JSONObject RequestParamsValidate = new JSONObject();
					RequestParamsValidate = new JSONObject();
					RequestParamsValidate.put("id", tempData.get("id").toString());
					RequestParamsValidate.put("otp", randomOTP);
					
					httpRequest.header("Content-Type", "application/json");
					httpRequest.body(RequestParamsValidate.toJSONString());		
					response = httpRequest.request(Method.POST, endpointValidate);
					parser = new JSONParser();
					json = (JSONObject) parser.parse(response.getBody().asString());
					responseBody = response.getBody().asString();
					responseStatus = json.get("status").toString();
					responseDescription = json.get("description").toString();
					
					logger.info("Response body : " + responseBody);
					logger.info("Response status : " + responseStatus);
					logger.info("Response Description : " + responseDescription);
					logger.info("Updated password for ID : " + tempData.get("id"));
					logger.info("Updated password for phone number : " + existingPhone);
					logger.info("Actual OTP : " + tempData.get("otp"));
					logger.info("Input OTP : " + randomOTP);
					Assert.assertTrue(responseBody != null);
					Assert.assertEquals(responseStatus, responseStatusFailed);
					Assert.assertEquals(responseDescription, responseOTPInvalid);
					Assert.assertEquals(response.getStatusCode(), 400);
				};
			}
			
		}else {
			Assert.assertTrue(false);
			logger.info("Request for reset password data " + RequestParamsReq  + " failed !");
		}		
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_007_TC004 --- ]");
	}
	
	// Forgot Password and reset to new password with not exist email
	@SuppressWarnings("unchecked")
	@Test(priority = 4)
	void TSC_007_TC005() throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_007_TC005 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParamsReq = new JSONObject();
		
		// REQUEST FORGOT PASSWORD BY PHONE NUMBER
		logger.info(" === Process request for reset password === ");
		RequestParamsReq.put("email", randomEmail);

		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsReq.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequest);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Request reset password for email : " + randomEmail);
		
		Assert.assertEquals(response.getStatusCode(), 404);	
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionAccountNotExist);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_007_TC005 --- ]");
	}
	
	// Forgot Password and reset to new password with not exist phone number
	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	void TSC_007_TC006() throws InterruptedException, ParseException {
		logger.info("[ --- Started TSC_007_TC006 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParamsReq = new JSONObject();
		
		// REQUEST FORGOT PASSWORD BY PHONE NUMBER
		logger.info(" === Process request for reset password === ");
		RequestParamsReq.put("phone", randomPhoneNumber);

		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsReq.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequest);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Request reset password for email : " + randomEmail);
		
		Assert.assertEquals(response.getStatusCode(), 404);	
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, responseDescriptionAccountNotExist);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_007_TC006 --- ]");
	}
	
	protected String getRandomNumber() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_007_ForgotPassword --- ]");
		System.out.println();
	} 
}