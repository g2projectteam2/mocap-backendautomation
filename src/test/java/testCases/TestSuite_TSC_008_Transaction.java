package testCases;

import org.testng.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_008_Transaction extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_008_Transaction.class);
	File getEmail, getPhone, getId, getName, getPassword, getStaticEmail, getStaticPhone;
	Scanner emailReader, phoneReader, idReader, nameReader, passwordReader, staticEmailReader, staticPhoneReader;
	String baseUrl = "";
	String endpointRequest = "/forgotPassword/request";
	String endpointValidate = "/forgotPassword/validate";
	String endpointUpdate = "/updatePassword";
	String endPointGetAllUser = "/all";
	String endPointBalancePayment = "/mobilecredit/balance";
	String endPointInsertBalance = "/insertBalance";
	String endPointBankTransfer = "/mobilecredit/banktransfer";
	String endPointHistoryTransaction = "/mobilecredit/history";
	String endPointValidatePayment = "/mobilecredit/validatetransfer";
	String endPointNonActivateAccount = "/setActiveStatus";
	boolean isBalanceDecrease = false;
	boolean isBalanceConsistent = false;
	int amountToBuy = 50000;
	int topUp = 100000;
	int initialBalance = 0;
	int balanceAfterTransaction =0;
	String invalidAmountToBuy = "-5000";
	String validAmountToBuy = "10000";
	String virtualAccountNumber = "";
	
	String transactionId = "";
	String provider = "xl";
	String phoneDestination = "087800001111";
	String existingEmail = "";
	String existingPhone = "";
	String existingId = "";
	String existingUserName = "";
	String existingUserPassword = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String staticEmail = "";
	String staticPhone = "";
	String newPassword = "NewPassword@"+getRandomNumber();
	String randomOTP = generateRandomOTP();
	String randomPhoneNumber = "0800"+getRandomNumber();
	String randomEmail = "Ujay"+generateRandomStringNumberForEmail()+"@random.com";

	
	String responseDescriptionTopUpSuccess = "user balance updated successfully";
	String responseOTPValid = "OTP valid";
	String responseOTPInvalid = "OTP invalid";
	String responseDescriptionInvalidEMail = "invalid email address";
	String responseDescriptionInvalidPassword = "invalid password";
	String responseDescriptionAccountNotExist = "invalid user";
	String responseDescriptionFailedProvider = "transaction failed in provider";
	String responseDescriptionNoPendingTransaction = "no pending transaction for this user";
	String responseDescriptionNonactiveUser = "user active status updated successfully";
	String responseDescriptionNonActiveUser = "this account has not been activated yet";
	
	String responseDescriptionBalancePaymentSuccess = "mobile credit top up success";
	String responseDescriptionBankTransferPaymentSuccess = "success processing transaction, please finish payment";
	
	String responseDescriptionNotEnoughBalance = "not enough balance";
	String responseDescriptionNotLoggedIn = "user not logged in";
	String responseDescriptionInvalidAmount = "invalid transaction amount";
	
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("\r\n");
		logger.info("[ --- Started TestSuite_TSC_008_Transaction --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			getName = new File("./assets/generatedData/userName.txt");
			getPassword = new File("./assets/generatedData/userPassword.txt");
			getStaticEmail = new File("./assets/generatedData/userEmailStatic.txt");
			getStaticPhone = new File("./assets/generatedData/userPhoneStatic.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			passwordReader = new Scanner(getPassword);
			nameReader = new Scanner(getName);
			staticEmailReader = new Scanner(getStaticEmail);
			staticPhoneReader = new Scanner(getStaticPhone);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
			while (nameReader.hasNextLine()) {
				String data = nameReader.nextLine();
				existingUserName = data;
			}
			nameReader.close();
			
			while (passwordReader.hasNextLine()) {
				String data = passwordReader.nextLine();
				existingUserPassword = data;
			}
			passwordReader.close();
			
			while (staticPhoneReader.hasNextLine()) {
				String data = staticPhoneReader.nextLine();
				staticPhone = data;
			}
			staticPhoneReader.close();
			
			while (staticEmailReader.hasNextLine()) {
				String data = staticEmailReader.nextLine();
				staticEmail = data;
			}
			staticEmailReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Make a transaction using balance payment method
	@SuppressWarnings("unchecked")
	@Test(priority = 0)
	void TSC_008_TC001() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC001 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParams = new JSONObject();

		logger.info(" === Process insert balance === ");
		RequestParams.put("id", existingId);
		RequestParams.put("balance", topUp);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endPointInsertBalance);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Insert balance to user id : " + existingId);
		logger.info("Total balance to insert : " + topUp);
		
		Assert.assertEquals(response.getStatusCode(), 200);	
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionTopUpSuccess);
		
		if(response.getStatusCode() == 200) {
			logger.info(" === Process insert balance success === ");
			
			// GET USER BALANCE TO INSERT TO TXT
			response = httpRequest.request(Method.GET, endPointGetAllUser);
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONArray jsonarray = new JSONArray();
			jsonarray = (JSONArray) json.get("users");
			
			for(int i = 0; i <=jsonarray.size()-1; i++) {
				JSONObject tempData = new JSONObject();
				tempData = (JSONObject) jsonarray.get(i);
				
				
				if(tempData.get("id").toString().equals(existingId)) {
					logger.info("Data user from Database : " + tempData);
					logger.info("=== Insert user balance to txt process ===");
					initialBalance = Integer.parseInt(tempData.get("balance").toString());
					try {
		                FileWriter generateUserBalance = new FileWriter("assets/generatedData/userBalance.txt");
		                generateUserBalance.write(tempData.get("balance").toString());
		                generateUserBalance.close();
		            } catch (IOException e) {
		                System.err.format("IOException: %s%n", e);
		            }
				};
			}
			
			JSONObject RequestParamsMakeTransaction = new JSONObject();

			logger.info(" === Process make transaction === ");
			RequestParamsMakeTransaction.put("userPhone", existingPhone);
			RequestParamsMakeTransaction.put("provider", provider);
			RequestParamsMakeTransaction.put("destinationPhone", phoneDestination);
			RequestParamsMakeTransaction.put("amount", amountToBuy);
			
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParamsMakeTransaction.toJSONString());
			response = httpRequest.request(Method.POST, endPointBalancePayment);
			
			JSONParser parserMakeTransaction = new JSONParser();
			JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
			String responseBodyMakeTransaction = response.getBody().asString();
			String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
			String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
			
			logger.info("Response body : " + responseBodyMakeTransaction);
			logger.info("Response status : " + responseStatusMakeTransaction);
			logger.info("Response Description : " + responseDescriptionMakeTransaction);
			logger.info("Transaction from : " + existingPhone);
			logger.info("Transaction to : " + phoneDestination);
			logger.info("Provider : " + provider);
			logger.info("Amount : " + amountToBuy);
			logger.info("Initial Balance : " + initialBalance);
			
			// GET TRANSACTION ID
			try {
				parser = new JSONParser();
				json = (JSONObject) parser.parse(response.getBody().asString());
				String tempData = json.get("transactionId").toString();
				transactionId = tempData.toString();
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
			
			Assert.assertEquals(response.getStatusCode(), 200);	
			Assert.assertTrue(responseBodyMakeTransaction != null);
			Assert.assertEquals(responseStatusMakeTransaction, responseStatusSuccess);
			Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionBalancePaymentSuccess);
			
			if(response.getStatusCode() == 200) {
				logger.info(" === Process make transaction success === ");
				logger.info("Transaction ID : "+transactionId);
				
				// Check if balance decrease after transaction
				if(balanceAfterTransaction <= initialBalance) {
					isBalanceDecrease = true;
				}
				
				// GET USER BALANCE TO INSERT TO TXT
				response = httpRequest.request(Method.GET, endPointGetAllUser);
				json = (JSONObject) parser.parse(response.getBody().asString());
				
				jsonarray = new JSONArray();
				jsonarray = (JSONArray) json.get("users");
				JSONObject tempData = new JSONObject();
				
				for(int i = 0; i <=jsonarray.size()-1; i++) {
					
					tempData = new JSONObject();
					tempData = (JSONObject) jsonarray.get(i);
					
					
					if(tempData.get("id").toString().equals(existingId)) {
						logger.info("Data user from Database : " + tempData);
						logger.info("=== Insert user balance after transaction to txt process ===");
						
						// Check if balance consistent
						if(Integer.parseInt(tempData.get("balance").toString()) == (initialBalance-amountToBuy)) {
							isBalanceConsistent = true;
						}
						
						
						try {
			                FileWriter generateUserBalance = new FileWriter("assets/generatedData/userBalance.txt");
			                generateUserBalance.write(tempData.get("balance").toString());
			                generateUserBalance.close();
			            } catch (IOException e) {
			                System.err.format("IOException: %s%n", e);
			            }
						balanceAfterTransaction = Integer.parseInt(tempData.get("balance").toString());
					};
				}
				logger.info("Balance after transaction : " + balanceAfterTransaction);
				Assert.assertEquals(balanceAfterTransaction, initialBalance-amountToBuy);		
			}else {
				logger.info(" === Process make transaction failed === ");
			}
		}else {
			logger.info(" === Process insert balance failed === ");
		}

		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC001 --- ]");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Make a transaction using bank transfer payment method
	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	void TSC_008_TC002() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC002 --- ]");
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
	
		JSONObject RequestParams = new JSONObject();

		logger.info(" === Process make transaction === ");
		RequestParams.put("userPhone", existingPhone);
		RequestParams.put("provider", provider);
		RequestParams.put("destinationPhone", phoneDestination);
		RequestParams.put("amount", amountToBuy);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endPointBankTransfer);
		
		JSONParser parserMakeTransaction = new JSONParser();
		JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
		String responseBodyMakeTransaction = response.getBody().asString();
		String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
		String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
		
		logger.info("Response body : " + responseBodyMakeTransaction);
		logger.info("Response status : " + responseStatusMakeTransaction);
		logger.info("Response Description : " + responseDescriptionMakeTransaction);
		logger.info("Transaction from : " + existingPhone);
		logger.info("Transaction to : " + phoneDestination);
		logger.info("Provider : " + provider);
		logger.info("Amount : " + amountToBuy);
		logger.info("Initial Balance : " + initialBalance);
		
		// GET VIRTUAL ACCOUNT NUMBER
		try {
			parserMakeTransaction = new JSONParser();
			jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
			String tempData = jsonMakeTransaction.get("vaNumber").toString();
			virtualAccountNumber = tempData.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
		
		Assert.assertEquals(response.getStatusCode(), 200);	
		Assert.assertTrue(responseBodyMakeTransaction != null);
		Assert.assertEquals(responseStatusMakeTransaction, responseStatusSuccess);
		Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionBankTransferPaymentSuccess);
		Assert.assertEquals(virtualAccountNumber.matches("[0-9]+") && virtualAccountNumber.length() == 16, true);
		
		if(response.getStatusCode() == 200) {
			logger.info(" === Process make transaction success === ");
			logger.info("Transaction ID : "+virtualAccountNumber);
			
			RequestParams = new JSONObject();

			logger.info(" === Process validate payment === ");
			RequestParams.put("userPhone", existingPhone);
			
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			response = httpRequest.request(Method.POST, endPointValidatePayment);
			
			JSONParser parserValidate = new JSONParser();
			JSONObject jsonValidate = (JSONObject) parserValidate.parse(response.getBody().asString());
			String responseBodyValidate= response.getBody().asString();
			String responseStatusValidate = jsonValidate.get("status").toString();
			String responseDescriptionValidate = jsonValidate.get("description").toString();
			
			Assert.assertEquals(response.getStatusCode(), 200);	
			Assert.assertTrue(responseBodyValidate != null);
			Assert.assertEquals(responseStatusValidate, responseStatusSuccess);
			Assert.assertEquals(responseDescriptionValidate, responseDescriptionBalancePaymentSuccess);
			
			logger.info("Response body : " + responseBodyValidate);
			logger.info("Response status : " + responseStatusValidate);
			logger.info("Response Description : " + responseDescriptionValidate);
			logger.info("Transaction from : " + existingPhone);
			logger.info("Transaction to : " + phoneDestination);
			logger.info("Provider : " + provider);
			logger.info("Amount : " + amountToBuy);
			logger.info("Initial Balance : " + initialBalance);
			
			if(response.getStatusCode() == 200) {
				logger.info(" === Process validate payment success === ");
			}else {
				logger.info(" === Process validate payment failed === ");
			}
			
			
			
			
			
			
		}else {
			logger.info(" === Process make transaction failed === ");
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC002 --- ]");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Check if data transaction is in database
	@Test(priority = 2)
	void TSC_008_TC003() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC003 --- ]");
		response = httpRequest.request(Method.GET, endPointHistoryTransaction + "?userid=" + existingId);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("transactions");
		
		int counterAssert = 0;
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) jsonarray.get(i);		
			
			if( tempData.get("id").toString().equals(transactionId) && Integer.parseInt(tempData.get("amount").toString()) == amountToBuy && tempData.get("destinationPhone").toString().equals(phoneDestination)){		
				counterAssert = 1;
				logger.info("Transaction with ID : " + transactionId + "found");
			}
		}
		Assert.assertEquals(counterAssert, 1);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC003 --- ]");
	}
	
	// Check if user amount decrease after transaction success
	@Test(priority = 3)
	void TSC_008_TC004() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC004 --- ]");
		Assert.assertEquals(isBalanceDecrease, true);
		if(isBalanceDecrease == true) {
			logger.info("User Balance decrease After Transaction");
		}else {
			logger.info("User Balance didn't decrease After Transaction");
		}
		logger.info("[ --- Finished TSC_008_TC004 --- ]");
	}
	
	// Check consistency between user balance with transaction
	@Test(priority = 4)
	void TSC_008_TC005() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC005 --- ]");
		Assert.assertEquals(isBalanceConsistent, true);
		if(isBalanceConsistent == true) {
			logger.info("User Balance consistent After Transaction");
		}else {
			logger.info("User Balance didn't consistent After Transaction");
		}
		logger.info("[ --- Finished TSC_008_TC005 --- ]");
	}
	
	
	// Make a transaction using balance payment method with not enough balance
	@SuppressWarnings("unchecked")
	@Test(priority = 5)
	void TSC_008_TC006() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC006 --- ]");
			
		JSONObject RequestParamsMakeTransaction = new JSONObject();
		logger.info(" === Process make transaction === ");
		RequestParamsMakeTransaction.put("userPhone", existingPhone);
		RequestParamsMakeTransaction.put("provider", provider);
		RequestParamsMakeTransaction.put("destinationPhone", phoneDestination);
		RequestParamsMakeTransaction.put("amount", amountToBuy);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsMakeTransaction.toJSONString());
		response = httpRequest.request(Method.POST, endPointBalancePayment);
		
		JSONParser parserMakeTransaction = new JSONParser();
		JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
		String responseBodyMakeTransaction = response.getBody().asString();
		String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
		String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
		
		logger.info("Response body : " + responseBodyMakeTransaction);
		logger.info("Response status : " + responseStatusMakeTransaction);
		logger.info("Response Description : " + responseDescriptionMakeTransaction);
		logger.info("Transaction from : " + existingPhone);
		logger.info("Transaction to : " + phoneDestination);
		logger.info("Provider : " + provider);
		logger.info("Initial Balance : " + initialBalance);
		logger.info("Amount to buy: " + amountToBuy);
		
		Assert.assertEquals(response.getStatusCode(), 400);	
		Assert.assertTrue(responseBodyMakeTransaction != null);
		Assert.assertEquals(responseStatusMakeTransaction, responseStatusFailed);
		Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionNotEnoughBalance);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC006 --- ]");
	}
	
	// Make a transaction using balance payment method when user not logged in
	@SuppressWarnings("unchecked")
	@Test(priority = 6)
	void TSC_008_TC007() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC007 --- ]");
			
		JSONObject RequestParamsMakeTransaction = new JSONObject();
		logger.info(" === Process make transaction === ");
		RequestParamsMakeTransaction.put("userPhone", existingPhone);
		RequestParamsMakeTransaction.put("provider", provider);
		RequestParamsMakeTransaction.put("destinationPhone", phoneDestination);
		RequestParamsMakeTransaction.put("amount", amountToBuy);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsMakeTransaction.toJSONString());
		response = httpRequest.request(Method.POST, endPointBalancePayment);
		
		JSONParser parserMakeTransaction = new JSONParser();
		JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
		String responseBodyMakeTransaction = response.getBody().asString();
		String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
		String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
		
		logger.info("Response body : " + responseBodyMakeTransaction);
		logger.info("Response status : " + responseStatusMakeTransaction);
		logger.info("Response Description : " + responseDescriptionMakeTransaction);
		logger.info("Transaction from : " + existingPhone);
		logger.info("Transaction to : " + phoneDestination);
		logger.info("Provider : " + provider);
		logger.info("Initial Balance : " + initialBalance);
		logger.info("Amount to buy: " + amountToBuy);
		
		Assert.assertEquals(response.getStatusCode(), 400);	
		Assert.assertTrue(responseBodyMakeTransaction != null);
		Assert.assertEquals(responseStatusMakeTransaction, responseStatusFailed);
		Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionNotLoggedIn);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC007 --- ]");
	}
	
	// Make a transaction using balance payment method when account is not exist
	@SuppressWarnings("unchecked")
	@Test(priority = 7)
	void TSC_008_TC008() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC008 --- ]");
			
		JSONObject RequestParamsMakeTransaction = new JSONObject();
		logger.info(" === Process make transaction === ");
		RequestParamsMakeTransaction.put("userPhone", getRandomPhone());
		RequestParamsMakeTransaction.put("provider", provider);
		RequestParamsMakeTransaction.put("destinationPhone", phoneDestination);
		RequestParamsMakeTransaction.put("amount", amountToBuy);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsMakeTransaction.toJSONString());
		response = httpRequest.request(Method.POST, endPointBalancePayment);
		
		JSONParser parserMakeTransaction = new JSONParser();
		JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
		String responseBodyMakeTransaction = response.getBody().asString();
		String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
		String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
		
		logger.info("Response body : " + responseBodyMakeTransaction);
		logger.info("Response status : " + responseStatusMakeTransaction);
		logger.info("Response Description : " + responseDescriptionMakeTransaction);
		logger.info("Transaction from : " + existingPhone);
		logger.info("Transaction to : " + phoneDestination);
		logger.info("Provider : " + provider);
		logger.info("Initial Balance : " + initialBalance);
		logger.info("Amount to buy: " + amountToBuy);
		
		Assert.assertEquals(response.getStatusCode(), 404);	
		Assert.assertTrue(responseBodyMakeTransaction != null);
		Assert.assertEquals(responseStatusMakeTransaction, responseStatusFailed);
		Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionAccountNotExist);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC008 --- ]");
	}
	
	// Make a transaction using balance payment method when account is not active
	@SuppressWarnings("unchecked")
	@Test(priority = 8)
	void TSC_008_TC009() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC009 --- ]");
		
		JSONObject RequestParams = new JSONObject();
		logger.info(" === Process make transaction === ");
		RequestParams.put("id", existingId);
		RequestParams.put("activeStatus", false);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endPointNonActivateAccount);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Nonactivate account with ID : " + existingId);
		
		Assert.assertEquals(response.getStatusCode(), 200);	
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, responseDescriptionNonactiveUser);
		
		if(response.getStatusCode() == 200) {
			logger.info("=== Nonactivate account success ===");
			
			logger.info("=== Process make transaction ===");
			JSONObject RequestParamsMakeTransaction = new JSONObject();
			logger.info(" === Process make transaction === ");
			RequestParamsMakeTransaction.put("userPhone", existingPhone);
			RequestParamsMakeTransaction.put("provider", provider);
			RequestParamsMakeTransaction.put("destinationPhone", phoneDestination);
			RequestParamsMakeTransaction.put("amount", validAmountToBuy);
			
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParamsMakeTransaction.toJSONString());
			response = httpRequest.request(Method.POST, endPointBalancePayment);
			
			JSONParser parserMakeTransaction = new JSONParser();
			JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
			String responseBodyMakeTransaction = response.getBody().asString();
			String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
			String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
			
			logger.info("Response body : " + responseBodyMakeTransaction);
			logger.info("Response status : " + responseStatusMakeTransaction);
			logger.info("Response Description : " + responseDescriptionMakeTransaction);
			logger.info("Transaction from : " + existingPhone);
			logger.info("Transaction to : " + phoneDestination);
			logger.info("Provider : " + provider);
			logger.info("Initial Balance : " + initialBalance);
			logger.info("Amount to buy: " + invalidAmountToBuy);
			
			Assert.assertEquals(response.getStatusCode(), 400);	
			Assert.assertTrue(responseBodyMakeTransaction != null);
			Assert.assertEquals(responseStatusMakeTransaction, responseStatusFailed);
			Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionNonActiveUser);
			
			
		}else {
			logger.info("=== Nonactivate account failed ===");
		}
			
		
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC009 --- ]");
	}
	
	// Make a transaction using balance payment method with invalid amount
	@SuppressWarnings("unchecked")
	@Test(priority = 9)
	void TSC_008_TC0010() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC0010 --- ]");
			
		JSONObject RequestParamsMakeTransaction = new JSONObject();
		logger.info(" === Process make transaction === ");
		RequestParamsMakeTransaction.put("userPhone", existingPhone);
		RequestParamsMakeTransaction.put("provider", provider);
		RequestParamsMakeTransaction.put("destinationPhone", phoneDestination);
		RequestParamsMakeTransaction.put("amount", invalidAmountToBuy);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsMakeTransaction.toJSONString());
		response = httpRequest.request(Method.POST, endPointBalancePayment);
		
		JSONParser parserMakeTransaction = new JSONParser();
		JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
		String responseBodyMakeTransaction = response.getBody().asString();
		String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
		String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
		
		logger.info("Response body : " + responseBodyMakeTransaction);
		logger.info("Response status : " + responseStatusMakeTransaction);
		logger.info("Response Description : " + responseDescriptionMakeTransaction);
		logger.info("Transaction from : " + existingPhone);
		logger.info("Transaction to : " + phoneDestination);
		logger.info("Provider : " + provider);
		logger.info("Initial Balance : " + initialBalance);
		logger.info("Amount to buy: " + invalidAmountToBuy);
		
		Assert.assertEquals(response.getStatusCode(), 400);	
		Assert.assertTrue(responseBodyMakeTransaction != null);
		Assert.assertEquals(responseStatusMakeTransaction, responseStatusFailed);
		Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionInvalidAmount);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC0010 --- ]");
	}
	
	// Make a transaction using balance payment method with not match provider and phone number
	@SuppressWarnings("unchecked")
	@Test(priority = 10)
	void TSC_008_TC0011() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC0011 --- ]");
			
		JSONObject RequestParamsMakeTransaction = new JSONObject();
		logger.info(" === Process make transaction === ");
		RequestParamsMakeTransaction.put("userPhone", existingPhone);
		RequestParamsMakeTransaction.put("provider", provider);
		RequestParamsMakeTransaction.put("destinationPhone", getRandomPhone());
		RequestParamsMakeTransaction.put("amount", invalidAmountToBuy);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsMakeTransaction.toJSONString());
		response = httpRequest.request(Method.POST, endPointBalancePayment);
		
		JSONParser parserMakeTransaction = new JSONParser();
		JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
		String responseBodyMakeTransaction = response.getBody().asString();
		String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
		String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
		
		logger.info("Response body : " + responseBodyMakeTransaction);
		logger.info("Response status : " + responseStatusMakeTransaction);
		logger.info("Response Description : " + responseDescriptionMakeTransaction);
		logger.info("Transaction from : " + existingPhone);
		logger.info("Transaction to : " + phoneDestination);
		logger.info("Provider : " + provider);
		logger.info("Initial Balance : " + initialBalance);
		logger.info("Amount to buy: " + invalidAmountToBuy);
		
		Assert.assertEquals(response.getStatusCode(), 400);	
		Assert.assertTrue(responseBodyMakeTransaction != null);
		Assert.assertEquals(responseStatusMakeTransaction, responseStatusFailed);
		Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionInvalidAmount);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC0011 --- ]");
	}
	
	// Validate payment with no transaction made
	@SuppressWarnings("unchecked")
	@Test(priority = 11)
	void TSC_008_TC0012() throws InterruptedException, ParseException {
		logger.info("\r\n");
		logger.info("[ --- Started TSC_008_TC0012 --- ]");
			
		JSONObject RequestParamsMakeTransaction = new JSONObject();
		RequestParamsMakeTransaction.put("userPhone", existingPhone);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsMakeTransaction.toJSONString());
		response = httpRequest.request(Method.POST, endPointValidatePayment);
		
		JSONParser parserMakeTransaction = new JSONParser();
		JSONObject jsonMakeTransaction = (JSONObject) parserMakeTransaction.parse(response.getBody().asString());
		String responseBodyMakeTransaction = response.getBody().asString();
		String responseStatusMakeTransaction = jsonMakeTransaction.get("status").toString();
		String responseDescriptionMakeTransaction = jsonMakeTransaction.get("description").toString();
		
		logger.info("Response body : " + responseBodyMakeTransaction);
		logger.info("Response status : " + responseStatusMakeTransaction);
		logger.info("Response Description : " + responseDescriptionMakeTransaction);
		logger.info("Validate Transaction from phone number : " + existingPhone);
		
		Assert.assertEquals(response.getStatusCode(), 400);	
		Assert.assertTrue(responseBodyMakeTransaction != null);
		Assert.assertEquals(responseStatusMakeTransaction, responseStatusFailed);
		Assert.assertEquals(responseDescriptionMakeTransaction, responseDescriptionNoPendingTransaction);
		Thread.sleep(5);
		logger.info("[ --- Finished TSC_008_TC0012 --- ]");
	}

	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_008_Transaction --- ]");
		System.out.println();
	}
	

}