package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import base.TestBase;

public class TestSuite_TSC_005_Profile extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TSC_005_Profile.class);
	File getEmail, getPhone, getId;
	Scanner emailReader, phoneReader, idReader;
	String baseUrl = "";
	String endpointTC01 = "/?id=";

	String existingEmail = "";
	String existingPhone = "";
	String existingId = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	
	String responseDescriptionInvalidEMail = "invalid email address";
	String responseDescriptionInvalidPassword = "invalid password";
	String responseDescriptionEmailExist = "email already registered";
	String responseDescriptionPhoneExist = "phone number already registered";
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TSC_005_Profile --- ]");
		
		try {
			getEmail = new File("./assets/generatedData/userEmail.txt");
			getPhone = new File("./assets/generatedData/userPhone.txt");
			getId = new File("./assets/generatedData/userId.txt");
			emailReader = new Scanner(getEmail);
			phoneReader = new Scanner(getPhone);
			idReader = new Scanner(getId);
			
			while (emailReader.hasNextLine()) {
				String data = emailReader.nextLine();
				existingEmail = data;
			}
			emailReader.close();
			
			while (phoneReader.hasNextLine()) {
				String data = phoneReader.nextLine();
				existingPhone = data;
			}
			phoneReader.close();
			
			while (idReader.hasNextLine()) {
				String data = idReader.nextLine();
				existingId = data;
			}
			idReader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	// Check specific existing user profile by id
	@Test(priority=0)
	void TC001() throws InterruptedException, ParseException {
		logger.info("[ --- Started TC001 --- ]");
		int testStatus = 0;
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpointTC01 + existingId);
		System.out.println( response.getBody().asString());
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);

		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("user");
		
		
		if(tempData.get("email").toString().equals(existingEmail) && tempData.get("phone").toString().equals(existingPhone)  ) {
			logger.info("Data user from Database : " + tempData);
			testStatus = 1;
		};
		
		Assert.assertEquals(testStatus, 1);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusSuccess);
		Assert.assertEquals(responseDescription, "user retrieved successfully");

		Thread.sleep(5);
		logger.info("[ --- Finished TC001 --- ]");
	}
	
	// Check specific not existing user profile
	@Test(priority=1)
	void TC002() throws InterruptedException, ParseException {
		logger.info("[ --- Started TC002 --- ]");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endpointTC01 + 00000);
		System.out.println( response.getBody().asString());
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);

		Assert.assertEquals(response.getStatusCode(), 404);
		Assert.assertTrue(responseBody != null);
		Assert.assertEquals(responseStatus, responseStatusFailed);
		Assert.assertEquals(responseDescription, "user not found");

		Thread.sleep(5);
		logger.info("[ --- Finished TC002 --- ]");
	}
	
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TSC_005_Profile --- ]");
		System.out.println();
	}
}